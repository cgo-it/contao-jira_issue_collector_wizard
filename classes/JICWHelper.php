<?php 
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2013
 * @author     Carsten Götzinger (info@cgo-it.de)
 * @package    jicw
 * @version    3.0.0 stable
 * @license    GNU/LGPL
 * @filesource
 */


namespace cgoIT\jicw;

/**
 * Class JICWHelper
 *
 * @copyright  cgo IT, 2013
 * @package    jicw
 */
class JICWHelper extends \Backend {

	/**
	 * Liefert den Namen des angemeldeten Backend-Redakteurs
	 */
	public function getUserFullName() {
		$this->import('BackendUser', 'User');
		return $this->User->name;
	}
	
	/**
	 * Liefert die E-Mail-Adresse des angemeldeten Backend-Redakteurs
	 */
	public function getUserEmail() {
		$this->import('BackendUser', 'User');
		return $this->User->email;
	}
	
	/**
	 * Liefert die installierten Module/Erweiterungen zurück
	 */
	public function getInstalledModules() {
		$retVal = '';
		$this->import('Database');
		$q = $this->Database->execute("select * from `tl_repository_installs` order by `extension`");
		while ($q->next()) $exts[] = (object)$q->row();
	
		foreach ($exts as $ext) {
			$retVal .= $ext->extension;
			$retVal .= ' (Version '.\Repository::formatCoreVersion($ext->version).')\n';
		}
		return $retVal;
	}
}

