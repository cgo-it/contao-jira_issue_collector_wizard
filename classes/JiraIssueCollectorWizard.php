<?php 
/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2013
 * @author     Carsten Götzinger (info@cgo-it.de)
 * @package    JiraIssueCollectorWizard
 * @version    3.0.0 stable
 * @license    GNU/LGPL
 * @filesource
 */

namespace cgoIT\jicw;

/**
 * Class JiraIssueCollectorWizard
 *
 * @copyright  cgo IT, 2013
 * @package    JiraIssueCollectorWizard
 */
class JiraIssueCollectorWizard extends \Widget
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_widget';
	
	/**
	 * URL to the Jira Issue Collector
	 * @var string
	 */
	protected $strIssueCollectorUrl = '';
	
	/**
	 * ID of the Jira Issue Collector
	 * @var string
	 */
	protected $strIssueCollectorId = '';
	 
	protected $arrCustomFields = array();
	 
	/**
	 * Initialize the object
	 * @param array
	*/
	public function __construct($arrAttributes = false) {
		parent::__construct($arrAttributes);
	}
	
	/**
	 * Add specific attributes
	 * @param string
	 * @param mixed
	 */
	public function __set($strKey, $varValue)
	{
		switch ($strKey)
		{
			case 'issueCollectorUrl':
				$arrResult = array();
				preg_match_all('/(.*collectorId=)(.+)/', $varValue, $arrResult);
				$this->strIssueCollectorUrl = $arrResult[1][0];
				$this->strIssueCollectorId = $arrResult[2][0];
				break;
				 
			case 'customFields':
				if (is_array($varValue)) {
					foreach ($varValue as $key=>$val) {
						if (is_array($val)) {
							$this->import($val[0]);
							$this->arrCustomFields[$key] = $this->$val[0]->$val[1]();
						} else {
							$this->arrCustomFields[$key] = $val;
						}
					}
				}
				break;
	
			default:
				parent::__set($strKey, $varValue);
				break;
		}
	}
	
	public function __get($strKey)
	{
		switch ($strKey)
		{
			default:
				return parent::__get($strKey);
				break;
		}
	}
	
	protected function validator($varInput) {
		return $varInput;
	}
	
	/**
	 * Generate the widget and return it as string
	 * @return string
	 */
	public function generate()
	{
		$strOutput = $this->generateDiv();
	
		return $strOutput;
	}
	
	/**
	 * Generates a div formatted MCW
	 * @param array
	 * @param array
	 * @param string
	 * @param array
	 * @return string
	 */
	protected function generateDiv()
	{
		$this->addJQuery();
		$return .= $this->addCollectorScript();
	
		$return .= '<div' . (($this->style) ? (' style="' . $this->style . '"') : '') . ' id="ctrl_' . $this->strId . '" class="tl_modulewizard jiraissuecollectorwizard">';
	
	
		$return .= '<input id="'.$this->strField.'" class="tl_submit" type="button" value="'.$this->label.'" name="'.$this->strField.'">';
	
		return $return . '</div>';
	}
	
	protected function addJQuery() {
		$GLOBALS['TL_JAVASCRIPT'][] = 'assets/jquery/core/' . JQUERY . '/jquery.min.js';
	}
	 
	protected function addCollectorScript() {
		$url = $this->strIssueCollectorUrl.$this->strIssueCollectorId;
		
		$str = <<<EOD
<script type="text/javascript">
        jQuery.noConflict();

        jQuery(document).ready(function () {
    		jQuery("#$this->strField").click(function (e) {
                e.preventDefault();
                
                jQuery.ajax({
				    url: "$url",
				    type: "get",
				    cache: true,
				    async: false,
				    dataType: "script"
				});
                
                showCollectorDialog_$this->strField();
            });
        });
        
        function showCollectorDialog_$this->strField() {
        	jQuery("#$this->strField").unbind('click');
			window.ATL_JQ_PAGE_PROPS = jQuery.extend(window.ATL_JQ_PAGE_PROPS, {
                // ============ Trigger Function ==============
                triggerFunction: function (showCollectorDialog) {
                   jQuery("#$this->strField").click(function(e) {
		    			e.preventDefault();
						showCollectorDialog();
				   });
				},
                // ==== we add the code below to set the field values ====
                fieldValues: {
					##fieldValues##
                }
            });

		    jQuery("#$this->strField").click();
	}
</script>
EOD;
		
		$fieldValues = '';
		foreach ($this->arrCustomFields as $key=>$value) {
			$fieldValues .= $key.' : "'.$value.'",
				';
		}
		
		$str = str_replace('##fieldValues##', $fieldValues, $str);
		
		return $str;
	}
	
}

