<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2013 Leo Feyer
 * 
 * @package Jira_issue_collector_wizard
 * @link    http://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'cgoIT',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'cgoIT\jicw\JICWHelper'               => 'system/modules/jira_issue_collector_wizard/classes/JICWHelper.php',
	'cgoIT\jicw\JiraIssueCollectorWizard' => 'system/modules/jira_issue_collector_wizard/classes/JiraIssueCollectorWizard.php',
));
